persona-metrics
==============


## Description

Cette bibliothèque propose de faire des comparaisons profils de recommandations à partir de jeu de données fournis.
On suppose que l'on fait plusieurs collectes (avec un `collect_id`) pour chaque profil (`persona_name`).
Pour chaque collecte et chaque contenu, on dispose de son nom (`recommandation_name`), 
par exemple le titre ou l'id pour une vidéo, ainsi que son rang dans la collecte (`rank`)

La bibliothèque prend en entrée une table avec les colones sus-nommées renseignées, 
et en sortie permet d'obtenir une matrice de distance entre profils ainsi qu'une représentation de la matrice sous forme de graphe (voir notebook)


## Installation

```bash
# Récupération du code avec Git
git clone ${GITLAB_URL}
cd persona-metrics

# Création d'un virtualenv et installation des dépendances requises
python3 -m virtualenv .venv
./.venv/bin/pip install poetry

# Installation des dépendances via poetry.
./.venv/bin/poetry install
```


## Utilisation

Un exemple est fourni dans le notebook [article_apvp.ipynb](notebooks%2Farticle_apvp.ipynb).




## Contribution

Avant de contribuer au dépôt, il est nécessaire d'initialiser les _hooks_ de _pre-commit_ :

```
pre-commit install
```


## Licence

Ce projet est sous licence MIT. Une copie intégrale du texte de la licence se trouve dans le fichier `LICENSE/mit.txt`.
