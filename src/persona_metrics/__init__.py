# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique peren.contact@finances.gouv.fr
#
# SPDX-License-Identifier: MIT

__version__ = "0.1"


from .core import (
    EDM2RecommandationComparator,
    SimpleRankedRecommandationComparator,
    RankedRecommandationComparator,
    TotalVariationRecommandationComparator,
)
