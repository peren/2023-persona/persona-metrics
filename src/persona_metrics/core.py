# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique peren.contact@finances.gouv.fr
#
# SPDX-License-Identifier: MIT

import os
from itertools import combinations
from typing import Dict, Iterable, List, Optional, Tuple, Union

import numpy as np
import pandas as pd
import polars as pl
from polars.type_aliases import IntoExpr
import ot

from .data_manipulation import (
    Column,
    UNIQUE_COLUMN,
    get_cost_matrix,
    get_cut_one_hot_dataset,
    get_mean_distance_dataframe,
    get_mean_values_and_ranks,
    get_one_hot_dataset,
)
from .edge_graph import build_chord_plot
from .plot_graph import make_graph
from .ranked_optimal_transport import (
    ComparableNumpyData,
    half_total_variation_distance,
    order_preserving_wasserstein,
    modified_emd2,
)


PathLike = Union[os.PathLike, str]


class RecommandationComparator:
    def __init__(
        self,
        recommandation_name: str = Column.recommandation_name,
        rank: str = Column.rank,
        persona_name: str = Column.persona_name,
        collect_id: str = Column.collect_id,
    ):
        """
        recommandation_dataframe should have the columns :
        - "persona_name" : a readable identifier of the persona
        - "rank" : the rank of the current recommandation in its collect
        - "collect_id" : a unique identifier for a given collect
        - "recommandation_column" : the recommandation given. It is used as a one-hot, so a hash of it works too

        usage
        > recommandation_comparator = RecommandationComparator()
        > recommandation_comparator.read_parquet("mon_fichier.parquet")
        > recommandation_comparator.compare_distributions()
        > recommandation_comparator.make_graph()
        """
        self.recommandation_name: str = recommandation_name
        self.rank: str = rank
        self.persona_name: str = persona_name
        self.collect_id: str = collect_id
        self.recommandation_dataframe = None
        self.compared_distribution = None
        self.mean_distances = None

    def _variables_cleanup_and_rename_columns(self):
        self.one_hot_dataset = None
        self.compared_distribution = None
        renaming_columns = {
            self.recommandation_name: Column.recommandation_name,
            self.rank: Column.rank,
            self.persona_name: Column.persona_name,
            self.collect_id: Column.collect_id,
        }
        self.recommandation_dataframe.rename(renaming_columns)

    def read_csv(self, csv_path: PathLike):
        self.recommandation_dataframe = pl.read_csv(csv_path)
        self._variables_cleanup_and_rename_columns()

    def read_parquet(self, parquet_path: PathLike):
        self.recommandation_dataframe = pl.read_parquet(parquet_path)
        self._variables_cleanup_and_rename_columns()

    def load_pandas(self, pandas_dataframe: pd.DataFrame):
        self.recommandation_dataframe = pl.from_pandas(pandas_dataframe)
        self._variables_cleanup_and_rename_columns()

    def load_polars(self, polars_dataframe: pl.DataFrame):
        self.recommandation_dataframe = polars_dataframe
        self._variables_cleanup_and_rename_columns()

    def _comparator_function_from_polars(
        self,
        first_distribution: pl.DataFrame,
        second_distribution: pl.DataFrame,
        cost_matrix: np.matrix,
        rank_first_distribution: pl.DataFrame,
        rank_second_distribution: pl.DataFrame,
    ):
        both_distributions = pl.concat([first_distribution, second_distribution]).mean()
        null_columns = [column for column in both_distributions.columns if both_distributions[column][0] == 0]
        size = first_distribution.drop(null_columns).shape[1]
        return self._comparator_function(
            comparable_numpy_data=ComparableNumpyData(
                first_distribution=first_distribution.drop(null_columns).to_numpy().reshape((-1,)),
                second_distribution=second_distribution.drop(null_columns).to_numpy().reshape((-1,)),
                cost_matrix=cost_matrix[:size, :size],
                rank_first_distribution=rank_first_distribution.drop(null_columns).to_numpy().tolist()[0],
                rank_second_distribution=rank_second_distribution.drop(null_columns).to_numpy().tolist()[0],
            )
        )

    def _comparator_function(
        self,
        comparable_numpy_data: ComparableNumpyData,
    ):
        raise NotImplementedError

    def compare_distributions(
        self,
        persona_name_list: Optional[List[str]] = None,
        one_hot_dataset: Optional[pl.DataFrame] = None,
        number_of_element_per_group: Optional[int] = None,
        number_of_groups: Optional[int] = None,
        max_number_of_collect_by_persona: Optional[int] = None,
    ) -> Dict[Tuple[str, str], float]:
        """
        Calculate a dict with {(persona_1, persona_2) : distance_between_distribution}
        for all pairs of personas considered

        :param persona_name_list: List of personas to compare. By default, compare all personas pairwise.
        :param one_hot_dataset: a dataframe with recommandations as one-hot. By default, recalculate it.
        :param number_of_element_per_group: number of elements by group if we wish to cut them into parts.
        :param number_of_groups: number of groups if we wish to cut them into parts.
        :param max_number_of_collect_by_persona: maximum number of collect to consider for each persona.
        Use if the number of collect for each persona is imbalanced.
        :return: a dict with {(persona_1, persona_2) : distance_between_distribution}
        for all pairs of personas considered.
        """
        if one_hot_dataset is None:
            raw_one_hot_dataset = get_one_hot_dataset(self.recommandation_dataframe)
            one_hot_dataset = get_cut_one_hot_dataset(
                raw_one_hot_dataset,
                number_of_element_per_group=number_of_element_per_group,
                number_of_groups=number_of_groups,
                max_number_of_collect_by_persona=max_number_of_collect_by_persona,
            )
        cost_matrix = get_cost_matrix(one_hot_dataset=one_hot_dataset)
        if UNIQUE_COLUMN in one_hot_dataset.columns:  # if there is rare content it is put in the first column
            one_hot_dataset = one_hot_dataset.select([pl.col(UNIQUE_COLUMN), pl.all().exclude(UNIQUE_COLUMN)])
        persona_name_list = persona_name_list or sorted(
            one_hot_dataset.select(pl.col(Column.persona_name), pl.col(Column.group_index)).unique().iter_rows()
        )
        mean_values, mean_ranks = get_mean_values_and_ranks(one_hot_dataset, persona_name_list=persona_name_list)
        self.compared_distribution = {
            (persona_name_1, persona_name_2): self._comparator_function_from_polars(
                mean_values[persona_name_1],
                mean_values[persona_name_2],
                cost_matrix,
                mean_ranks[persona_name_1],
                mean_ranks[persona_name_2],
            )
            for persona_name_1, persona_name_2 in combinations(persona_name_list, 2)
        } | {(persona_name, persona_name): 0.0 for persona_name in persona_name_list}
        return self.compared_distribution

    def make_report(
        self,
        number_of_element_per_group: Optional[int] = None,
        number_of_groups: Optional[int] = None,
        show_graph: bool = True,
        show_cord: bool = True,
        show_table: bool = True,
        graph_title: Optional[str] = None,
        max_number_of_collect_by_persona: Optional[int] = None,
        save_graph: bool = False,
    ):
        """
        show the results of a distance analysis of the given recommandations

        :param number_of_element_per_group: number of elements by group if we wish to cut them into parts
        :param number_of_groups: number of groups if we wish to cut them into parts
        :param show_graph: Do we show a heatmap of the distances. Defaults to True
        :param show_cord: Do we show the cord distance graph. Defaults to True
        :param show_table: Do we show the cord distance graph. Defaults to True
        :param graph_title: The title for the cord graph. Defaults to no title
        :param max_number_of_collect_by_persona: maximum number of collect to consider for each persona.
        :param save_graph: Do we save the cord graph to a file. Default to False.
        :return: display things depending on the choice made with the arguments
        """
        os.environ["POLARS_FMT_MAX_ROWS"] = "200"
        if number_of_element_per_group is None and number_of_groups is None:
            number_of_groups = 2
        distribution = self.compare_distributions(
            number_of_element_per_group=number_of_element_per_group,
            number_of_groups=number_of_groups,
            max_number_of_collect_by_persona=max_number_of_collect_by_persona,
        )
        self.mean_distances = get_mean_distance_dataframe(distribution)
        if show_cord:
            build_chord_plot(
                self.mean_distances.sort(
                    [
                        pl.col("mean distance").min().over("cluster_1").alias("min_1"),
                        pl.col("mean distance").min().over("cluster_2").alias("min_2"),
                    ]
                ),
                graph_title=graph_title,
                max_color_value=1,
                save_graph=save_graph,
            )
        if show_graph:
            make_graph(compared_distribution=distribution)
        if show_table:
            print(
                self.mean_distances.select(
                    [
                        pl.all().exclude(["mean distance", "standard deviation"]),
                        (100 * pl.col("mean distance")).alias("mean distance adjusted"),
                        (100 * pl.col("standard deviation")).alias("standard deviation adjusted"),
                    ]
                )
            )

    def redraw_graph(
        self,
        sort: Optional[Union[IntoExpr, Iterable[IntoExpr]]] = None,
        edges_attenuation_factor: int = 1,
        save_graph: bool = False,
    ):
        if sort is None:
            sort = [
                pl.col("mean distance").min().over("cluster_1").alias("min_1"),
                pl.col("mean distance").min().over("cluster_2").alias("min_2"),
            ]
        build_chord_plot(
            self.mean_distances.sort(sort), edges_attenuation_factor=edges_attenuation_factor, save_graph=save_graph
        )


class EDM2RecommandationComparator(RecommandationComparator):
    def _comparator_function(self, comparable_numpy_data: ComparableNumpyData):
        return ot.lp.emd2(
            comparable_numpy_data.first_distribution,
            comparable_numpy_data.second_distribution,
            comparable_numpy_data.cost_matrix,
        )


class RankedRecommandationComparator(RecommandationComparator):
    def _comparator_function(
        self,
        comparable_numpy_data: ComparableNumpyData,
    ):
        return order_preserving_wasserstein(comparable_numpy_data)


class SimpleRankedRecommandationComparator(RecommandationComparator):
    def _comparator_function(
        self,
        comparable_numpy_data: ComparableNumpyData,
    ):
        return modified_emd2(comparable_numpy_data)


class TotalVariationRecommandationComparator(RecommandationComparator):
    def _comparator_function(
        self,
        comparable_numpy_data: ComparableNumpyData,
    ):
        return half_total_variation_distance(comparable_numpy_data)
