# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique peren.contact@finances.gouv.fr
#
# SPDX-License-Identifier: MIT

from typing import Optional

import numpy as np
import polars as pl


class Column:
    """
    The names columns will be renamed to
    """

    recommandation_name = "recommandation_name"
    rank = "rank"
    persona_name = "persona_name"
    collect_id = "collect_id"
    one_hot_prefix = "category"
    group_index = "group_index"
    persona_index = "persona_index"
    unique_recommandation = "unique_recommandation"


UNIQUE_COLUMN = f"{Column.one_hot_prefix}_{Column.unique_recommandation}"


def get_one_hot_dataset(recommandation_dataframe):
    """
    perform a one-hot on "recommandation_name", maps all unique values to "unique_recommandation"
    """
    one_hot_dataset = recommandation_dataframe.select(
        [
            pl.col(Column.collect_id),
            pl.col(Column.rank),
            pl.col(Column.persona_name),
            pl.when(pl.count().over(Column.recommandation_name) == 1)
            .then(Column.unique_recommandation)
            .otherwise(pl.col(Column.recommandation_name))
            .alias(Column.one_hot_prefix),
        ]
    ).to_dummies(columns=[Column.one_hot_prefix])
    return one_hot_dataset


def get_cost_matrix(one_hot_dataset):
    pure_one_hot_dataset = one_hot_dataset.drop([Column.collect_id, Column.persona_name])
    _number_of_lines, number_of_categories = pure_one_hot_dataset.shape
    cost_matrix = np.ones((number_of_categories, number_of_categories)) - np.identity(n=number_of_categories)
    if UNIQUE_COLUMN in one_hot_dataset.columns:
        cost_matrix[0][0] = 1.0  # the cost here is one because all videos in the class are different
    return cost_matrix


def get_cut_one_hot_dataset(
    one_hot_dataset: pl.DataFrame,
    number_of_element_per_group: Optional[int] = None,
    number_of_groups: Optional[int] = None,
    max_number_of_collect_by_persona: Optional[int] = None,
):
    if number_of_element_per_group is None and number_of_groups is None and max_number_of_collect_by_persona is None:
        return one_hot_dataset.select(pl.all(), pl.lit("0").alias("group_index"))
    if number_of_element_per_group is not None and number_of_groups is not None:
        raise RuntimeError(
            f"You can't have both {number_of_element_per_group=} and {number_of_groups=} not equals to None"
        )
    cut_to_max = one_hot_dataset.select(
        [
            pl.col(Column.collect_id).rank(method="dense").over([Column.persona_name]).alias(Column.persona_index) - 1,
            pl.all(),
        ]
    )
    if max_number_of_collect_by_persona:
        cut_to_max = cut_to_max.filter(pl.col(Column.persona_index) < max_number_of_collect_by_persona)
    persona_group = (
        (pl.col(Column.persona_index) // number_of_element_per_group)
        if number_of_element_per_group
        else (pl.col(Column.persona_index) % number_of_groups)
    )
    return cut_to_max.select(
        [
            pl.col(Column.persona_name),
            persona_group.alias(Column.group_index),
            pl.all().exclude([Column.persona_name, Column.persona_index]),
        ]
    )


def get_mean_values_and_ranks(one_hot_dataset, persona_name_list):
    mean_values = {
        (persona_name, group_index): one_hot_dataset.filter(
            (pl.col(Column.persona_name) == persona_name) & (pl.col(Column.group_index) == group_index)
        )
        .mean()
        .drop([Column.persona_name, Column.collect_id, Column.group_index, Column.rank])
        for persona_name, group_index in persona_name_list
    }
    one_hot_columns = one_hot_dataset.drop([Column.persona_name, Column.collect_id]).columns
    max_rank = one_hot_dataset[Column.rank].max()
    mean_ranks = {
        (persona_name, group_index): one_hot_dataset.filter(
            (pl.col(Column.persona_name) == persona_name) & (pl.col(Column.group_index) == group_index)
        )
        .select(
            [
                (
                    pl.when(pl.col(one_hot_column) > 0).then(pl.col(Column.rank)).otherwise(0).sum()
                    / pl.when(pl.col(one_hot_column) > 0).then(1).otherwise(0).sum()
                ).alias(one_hot_column)
                for one_hot_column in one_hot_columns
            ]
        )
        .fill_nan(2 * max_rank)
        for persona_name, group_index in persona_name_list
    }
    return mean_values, mean_ranks


def get_mean_distance_dataframe(compared_distribution):
    distance_dataframe = pl.DataFrame(
        [
            {
                "cluster_1": f"{persona_name_1}|{group_index_1}",
                "cluster_2": f"{persona_name_2}|{group_index_2}",
                "distance": distance,
            }
            for (
                (persona_name_1, group_index_1),
                (persona_name_2, group_index_2),
            ), distance in compared_distribution.items()
        ]
    )
    distances_agg = distance_dataframe.filter(pl.col("distance") != 0.0).select(
        [
            pl.col("distance").alias("agg_distance"),
            pl.col("cluster_1").str.extract("([^|]+)|", 0),
            pl.col("cluster_2").str.extract("([^|]+)|", 0),
        ]
    )
    distances = (
        distances_agg.groupby(["cluster_1", "cluster_2"])
        .agg(
            [
                pl.col("agg_distance").mean().alias("mean distance"),
                pl.col("agg_distance").std().alias("standard deviation"),
            ]
        )
        .sort(["cluster_1", "cluster_2"])
    )
    return distances
