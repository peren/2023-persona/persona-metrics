# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique peren.contact@finances.gouv.fr
#
# SPDX-License-Identifier: MIT

from typing import Optional

import matplotlib.pyplot as plt
import networkx as nx
from matplotlib import cm, colors
import polars as pl


def build_chord_plot(
    mean_distance_dataframe: pl.DataFrame,
    sort_by_column=None,
    default_node_size: int = 1000,
    default_edge_width: int = 10,
    scale_labels_pos: float = 1.4,
    edges_attenuation_factor: int = 1,
    node_cmap_name="YlGnBu",
    figsize=(10, 7),
    graph_title: Optional[str] = None,
    max_color_value: Optional[int] = None,
    save_graph=False,
):
    """
    Plot a chord diagram of persona measures

    :params mean_distance_dataframe: polars dataframe containing persona measures
    :params sort_by_column: None or string, if string must match one of df columns
    :params default_node_size: int to adjust node size
    :params default_edge_width: int to adjust edges width
    :params scale_labels_pos: float to adjust labels position
    :params edges_attenuation_factor:
    float >= 0 to adjust alpha of edges. 0 for no alpha and +inf for total transparency
    :params node_cmap_name: cmap for nodes, look at matplotlib colormaps
    :params edges_cmap_name: cmap for edges, look at matplotlib colormaps
    :params figsize: Size of the figure
    :params graph_title: Title for the graph
    :params max_color_value: Max value for the color scaling
    """
    # Organize data :
    similarity_dataframe = mean_distance_dataframe.select(
        [pl.all(), (1 - pl.col("mean distance")).alias("similarity weight")]
    )

    if sort_by_column is not None:
        similarity_dataframe = similarity_dataframe.sort(sort_by_column)
    similarity_graph = nx.from_pandas_edgelist(
        similarity_dataframe,
        "cluster_1",
        "cluster_2",
        ["mean distance", "standard deviation", "similarity weight"],
    )
    # Do not plot self-referencing edges
    # Self referencing infos used for node color and size
    similarity_graph_without_loop = nx.from_edgelist(
        [(node_a, node_b) for node_a, node_b in similarity_graph.edges() if node_a != node_b]
    )

    # Compute positions and values :
    node_positions = nx.circular_layout(similarity_graph)
    labels_positions = nx.circular_layout(similarity_graph, scale=scale_labels_pos)

    loop_similarities = [
        similarity_graph.get_edge_data(node, node)["similarity weight"] for node in similarity_graph.nodes
    ]
    node_vmax = max(loop_similarities) if max_color_value is None else max_color_value
    node_size = [default_node_size * (loop_similarity / node_vmax) for loop_similarity in loop_similarities]
    node_norm = colors.Normalize(vmin=0, vmax=node_vmax)
    node_cmap = cm.get_cmap(node_cmap_name)
    node_colors = [node_cmap(node_norm(loop_similarity)) for loop_similarity in loop_similarities]

    non_loop_similarities = [
        similarity_graph.get_edge_data(node_a, node_b)["similarity weight"]
        for (node_a, node_b) in similarity_graph_without_loop.edges
    ]
    edges_vmax = max(non_loop_similarities)
    edges_width = [
        default_edge_width * (non_loop_similarity / edges_vmax) for non_loop_similarity in non_loop_similarities
    ]
    edges_alpha = [pow(s / edges_vmax, edges_attenuation_factor) for s in non_loop_similarities]
    edges_cmap = cm.get_cmap(node_cmap_name)
    edges_colors = [edges_cmap(node_norm(non_loop_similarity)) for non_loop_similarity in non_loop_similarities]
    edges_colors = [(r, g, b, a) for (r, g, b, _), a in zip(edges_colors, edges_alpha)]

    # Draw plot
    _fig, ax = plt.subplots(figsize=figsize)
    nx.draw_networkx_nodes(
        similarity_graph,
        pos=node_positions,
        node_size=node_size,
        node_color=node_colors,
        margins=0.5,
        edgecolors="k",
        linewidths=0.5,
    )
    nx.draw_networkx_edges(
        similarity_graph_without_loop,
        pos=node_positions,
        arrows=True,
        connectionstyle="arc3,rad=.2",
        edge_cmap=edges_cmap,
        alpha=edges_alpha,
        edge_color=edges_colors,
        width=edges_width,
    )
    nx.draw_networkx_labels(similarity_graph, pos=labels_positions)

    # Draw legends
    node_sm = plt.cm.ScalarMappable(cmap=node_cmap, norm=node_norm)
    node_sm._A = []  # pylint: disable=protected-access
    plt.colorbar(node_sm, ax=ax, label="Similarity of sets of recommandation")

    if graph_title:
        plt.title(graph_title)
    plt.tight_layout()
    if save_graph:
        plt.savefig(f"{graph_title}_graph.png")
    plt.show()
