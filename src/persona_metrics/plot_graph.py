# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique peren.contact@finances.gouv.fr
#
# SPDX-License-Identifier: MIT
from typing import Dict, Tuple

import altair as alt
import polars as pl


def make_graph(compared_distribution: Dict[Tuple[str, str], float], graph_width=400, graph_height=400):
    """
    Fait une heatmap à partir des distances de distributions fournies

    :param compared_distribution: dictionary with the distance between each pair of personas
    :param graph_width: graph width
    :param graph_height: graph height
    :return:
    """
    alt.data_transformers.disable_max_rows()
    distance_dataframe = pl.DataFrame(
        [
            {
                "cluster_1": f"{persona_name_1}|{group_index_1}",
                "cluster_2": f"{persona_name_2}|{group_index_2}",
                "distance": distance,
            }
            for (
                (persona_name_1, group_index_1),
                (persona_name_2, group_index_2),
            ), distance in compared_distribution.items()
        ]
    )
    chart_distance = (
        alt.Chart(distance_dataframe.to_pandas())
        .mark_rect()
        .encode(
            x=alt.X("cluster_2:O"),
            y=alt.Y("cluster_1:O"),
            color="distance:Q",
        )
        .properties(
            title="heatmap de distance",
            width=graph_width,
            height=graph_height,
        )
    )
    distances_agg = distance_dataframe.filter(pl.col("distance") != 0.0).select(
        [
            pl.col("distance").alias("agg_distance"),
            pl.col("cluster_1").str.extract("([^|]+)|", 0),
            pl.col("cluster_2").str.extract("([^|]+)|", 0),
        ]
    )
    sort_order = [
        persona_name.strip("|")
        for persona_name in sorted([persona_name + "|" for persona_name in distances_agg["cluster_1"].to_list()])
    ]
    chart_agg_distances = (
        alt.Chart(distances_agg.groupby(["cluster_1", "cluster_2"]).mean().to_pandas())
        .mark_rect()
        .encode(
            x=alt.X("cluster_2:O", sort=sort_order),
            y=alt.Y("cluster_1:O", sort=sort_order),
            color=alt.Color("agg_distance:Q"),
        )
        .properties(
            title="heatmap de distance agrégée",
            width=graph_width,
            height=graph_height,
        )
    )
    plot = alt.vconcat(chart_distance, chart_agg_distances)
    plot.display()
