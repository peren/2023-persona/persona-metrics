# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique peren.contact@finances.gouv.fr
#
# SPDX-License-Identifier: MIT

from dataclasses import dataclass

import numpy as np
import ot

FLOAT_PRECISION = np.sqrt(7.0 / 3 - 4.0 / 3 - 1)


@dataclass
class ComparableNumpyData:
    first_distribution: np.array
    second_distribution: np.array
    cost_matrix: np.matrix
    rank_first_distribution: np.array
    rank_second_distribution: np.array


def kullback_leibler_divergence(p, q):
    return np.sum(np.where(p != 0, p * np.log(p / q), 0))


def order_preserving_wasserstein_vanilla(input_data: ComparableNumpyData, l1=1, l2=1, sigma=1):
    """
    Implémentation de https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjI4I2h9aj9AhVgUaQEHW84AtsQFnoECAkQAQ&url=https%3A%2F%2Fopenaccess.thecvf.com%2Fcontent_cvpr_2017%2Fpapers%2FSu_Order-Preserving_Wasserstein_Distance_CVPR_2017_paper.pdf&usg=AOvVaw1sFN1YzDKoAEQZ0VljKPAK  # noqa
    Très très inspiré de https://pythonot.github.io/gen_modules/ot.bregman.html?highlight=sinkhorn_knopp%20b%20m%20reg#ot.bregman.sinkhorn_epsilon_scaling # noqa
    """
    rank_a = input_data.first_distribution if input_data.first_distribution is not None else []
    rank_b = input_data.second_distribution if input_data.second_distribution is not None else []
    a, b, distance_matrix = ot.utils.list_to_array(
        input_data.first_distribution, input_data.second_distribution, input_data.cost_matrix
    )

    # old magic for numerical stability
    l2 = l2 + FLOAT_PRECISION
    sigma_sq = sigma**2 + FLOAT_PRECISION
    a[a < FLOAT_PRECISION] = FLOAT_PRECISION
    b[b < FLOAT_PRECISION] = FLOAT_PRECISION

    # the sum of the marginals must be equal, not only to 1 : let's avoid numerical errors
    sum_a = sum(a)
    sum_b = sum(b)
    a = a * (sum_a + sum_b)
    a = a / sum_a
    b = b * (sum_a + sum_b)
    b = b / sum_b

    nx = ot.utils.get_backend(distance_matrix, a, b)

    if len(a) == 0:
        a = nx.full((distance_matrix.shape[0],), 1.0 / distance_matrix.shape[0], type_as=distance_matrix)
    if len(b) == 0:
        b = nx.full((distance_matrix.shape[1],), 1.0 / distance_matrix.shape[1], type_as=distance_matrix)

    # init data
    dim_a = len(a)
    dim_b = b.shape[0]

    # compute rank adjusted cost matrix
    if len(rank_a) == 0:
        N = np.tile(np.arange(1, dim_b + 1), (dim_a, 1)) / dim_b
        M = np.tile(np.arange(1, dim_a + 1), (dim_b, 1)).T / dim_a
    else:
        N = (np.tile(rank_b, (dim_a, 1)) + 1) / dim_b
        M = (np.tile(rank_a, (dim_b, 1)) + 1).T / dim_a
    distance_to_diagonal = np.abs(N - M) / np.sqrt(1 / N**2 + 1 / M**2)
    prior_distribution = np.exp(-(distance_to_diagonal**2) / (2 * sigma_sq)) / np.sqrt(sigma_sq * 2 * np.pi)
    prior_distribution = prior_distribution / sum(sum(prior_distribution))
    inverse_difference = l1 / ((1 / N - 1 / M) ** 2 + 1)

    # Optimal transport computation
    # It is necessary to switch to the log space to use directly the POT backend
    log_rank_adjusted_cost_matrix = np.log(prior_distribution) + ((inverse_difference - distance_matrix) / l2)
    transport_matrix = ot.bregman.sinkhorn_stabilized(a, b, log_rank_adjusted_cost_matrix, -1.0)

    # compute the loss
    transport_matrix = transport_matrix / (sum_a + sum_b)
    inverse_difference_moment = np.sum(transport_matrix / ((1 / N - 1 / M) ** 2 + 1))  # noqa
    return (
        np.trace(np.dot(transport_matrix.T, distance_matrix))
        - l1 * inverse_difference_moment
        + l2 * kullback_leibler_divergence(transport_matrix, prior_distribution)
    )


def order_preserving_wasserstein(input_data: ComparableNumpyData, l1=None, l2=None, sigma=None):
    """implementation of order_preserving_wasserstein with default good set of parameters"""
    rank_a = input_data.rank_first_distribution if input_data.rank_first_distribution is not None else []
    rank_b = input_data.rank_second_distribution if input_data.rank_second_distribution is not None else []
    dim_a = len(input_data.first_distribution) if len(rank_a) == 0 else max(rank_a)
    dim_b = input_data.second_distribution.shape[0] if len(rank_a) == 0 else max(rank_b)
    # bandwidth of the Gaussian (6 sigma) is equal to one quarter of the rank scale
    sigma = (1.0 / 4) * np.sqrt(dim_a * dim_b) / 6 if sigma is None else sigma
    # same order of magnitude for l1 and l2
    l1 = l1 or 1
    l2 = l2 or 1
    input_data.rank_first_distribution = rank_a
    input_data.rank_second_distribution = rank_b
    return order_preserving_wasserstein_vanilla(input_data, l1, l2, sigma)


def modified_emd2(input_data: ComparableNumpyData, sigma=None):
    """implementation of order_preserving_wasserstein with default good set of parameters"""
    dim_a = len(input_data.first_distribution)
    dim_b = input_data.second_distribution.shape[0]
    rank_max = max(input_data.rank_second_distribution + input_data.rank_first_distribution)
    # bandwidth of the Gaussian (6 sigma) is equal to one quarter of the rank scale
    sigma = (1.0 / 4) * rank_max / 6 if sigma is None else sigma

    rank_a_matrix = np.tile(input_data.rank_second_distribution, (dim_a, 1))
    rank_b_matrix = np.tile(input_data.rank_first_distribution, (dim_b, 1)).T
    adjusted_cost = np.square(rank_a_matrix - rank_b_matrix)
    gaussian_constant = 1 / np.sqrt(sigma * 2 * np.pi)
    gaussian_distance_cost = np.exp(-(adjusted_cost**2) / (2 * sigma)) * gaussian_constant
    modified_cost_matrix = input_data.cost_matrix - gaussian_distance_cost + np.amax(gaussian_distance_cost)
    return ot.lp.emd2(input_data.first_distribution, input_data.second_distribution, modified_cost_matrix)


def half_total_variation_distance(input_data: ComparableNumpyData):
    """fast calculation of wasserstein for the case where the distance matrix is 1 - ID or similar"""
    min_values = np.min(
        np.concatenate([input_data.first_distribution, input_data.second_distribution]).reshape((2, -1)), axis=0
    )
    diagonal_values = 1 - input_data.cost_matrix.diagonal()
    min_values_without_diagonal = np.multiply(min_values, diagonal_values)
    return 1 - np.sum(min_values_without_diagonal)
