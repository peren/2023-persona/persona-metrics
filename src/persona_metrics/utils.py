# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique peren.contact@finances.gouv.fr
#
# SPDX-License-Identifier: MIT

from typing import Dict, Tuple

import polars as pl


def get_timestamp_mappings(dataframe_with_timestamps: pl.DataFrame) -> Tuple[Dict[str, str], Dict[str, int]]:
    """

    :param dataframe_with_timestamps: a dataframe with a "persona_name" column and a "timestamp" column, with
    persona_name like "n_hour"
    :return: a dict {persona_name: %d_%H} and a dict {%d_%H: number_of_h_from_the_start}
    """
    personas_start_timestamps = dataframe_with_timestamps.groupby("persona_name").agg([pl.col("timestamp").min()])
    start_timestamp_by_persona = {
        groupby_values["persona_name"]: groupby_values["timestamp"]
        for groupby_values in personas_start_timestamps.to_dicts()
    }
    start_date_by_persona = {
        persona: timestamp.strftime("%d_%H") for persona, timestamp in start_timestamp_by_persona.items()
    }
    timing_to_distance = {timestamp: int(persona.split("_")[0]) for persona, timestamp in start_date_by_persona.items()}
    return start_date_by_persona, timing_to_distance


def augment_mean_with_time(
    mean_distances_dataframes: pl.DataFrame, start_date_by_persona: Dict[str, str], timing_to_distance: Dict[str, int]
):
    hour_mean_distance = mean_distances_dataframes.select(
        pl.col("cluster_1").map_dict(start_date_by_persona),
        pl.col("cluster_2").map_dict(start_date_by_persona),
        pl.all().exclude("cluster_1", "cluster_2"),
    )
    hour_mean_distance_with_time = hour_mean_distance.select(
        [
            pl.all(),
            (pl.col("cluster_2").map_dict(timing_to_distance) - pl.col("cluster_1").map_dict(timing_to_distance))
            .abs()
            .alias("time_distance"),
        ]
    )
    return hour_mean_distance_with_time
